//Setup dependencies
const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors")
require ("dotenv").config () //initialize env


//Import routes
const user_routes = require("./routes/userRoutes")
const course_routes = require("./routes/courseRoutes")


//MongoDB connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@wdc028-course-booking.sevh8po.mongodb.net/course-booking-db?retryWrites=true&w=majority`,{
	useNewUrlParser: true,
	useUnifiedTopology: true
})


mongoose.connection.once('open', () => console.log("Connected to MongoDB!"))


//Server setup
const app = express()


//Middlewar
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:true}))


//Routes
app.use("/users", user_routes)
app.use("/courses", course_routes)

//Server listening
//To dynamically switch from the port to the localhost to the port of the cloud server, we have to use an OR operator to check which one express.js will use
app.listen(process.env.PORT || 3000, () => {
	console.log(`API is now running on port ${process.env.PORT || 3000}`)
})