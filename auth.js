//Store files for JWT
//like a controller for JWT

const jwt = require("jsonwebtoken")

const secret = "B271CourseBookingAPI" //secret key to be used for validating the token


//Method for generating a token with JWT
module.exports.createAccessToken = (user) => {
	const user_data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(user_data, secret, {})
}


//Verify the validity of token
//custom middleware (na ikakabit sa routes <userRoutes.js>)
module.exports.verify = (request, response, next) => {

	//If the token exists, then slice its first 7 characters to remove the default 'Bearer' text when using the request.headers.authorization property. What will be left if only the tokn itsself.
	let token = request.headers.authorization

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length)

		//The verify funtion will check the token and the secret key used in this application to verify where it came from
		return jwt.verify(token, secret, (error, data) => {
			//If there is an error it will return an object saying that the authentication has failed
			if(error){
				return response.send({auth: "Verification failed."})
			}

			//but if there is no error it will exit this function and move on to the next function
			next()
		})
	}else {
		return response.send({auth: "Token is not defined."})
	}
}


//To get user data from the token
module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length)

		//Before decoding and getting the user data from the token, it first wll verify it using the secret key
		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return null
			}

			//After token has been verified, it runs the code function from JWT which will then be able to access its payload (user data)
			return jwt.decode(token, {complete: true}).payload
		})
	}else {
		return null
	}
}