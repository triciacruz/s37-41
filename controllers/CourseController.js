const Course = require ("../models/Course")


//Get all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(active_courses => {
		return active_courses
	})
}


//Create new course
module.exports.createCourse = (request_body) => {
	
	let new_course = new Course({
		name: request_body.name,
		description: request_body.description,
		price:request_body.price
	})

	return new_course.save().then((created_course, error) => {
		
		if(error){
			return error
		}

		/*
		return "Course created successfully!"
		*/

		return {
			message: "Course created successfully!",
			data: created_course
		}
		
	})
}


//Get single course
module.exports.getCourse = (course_id) => {
	return Course.findById(course_id).then(result => {
		return result
	})
}


//Update existing courses
module.exports.updateCourse = (course_id, new_content) => {

	let updated_course = {
		name: new_content.name,
		description: new_content.description,
		price:new_content.price
	}

	return Course.findByIdAndUpdate(course_id, updated_course).then((modified_course, error) => {
		
		if(error){
			return error
		}

		return {
			message: "Course updated successfully!",
			data: modified_course
		}
	})
}

//My answer

//Get all courses
module.exports.getAll = () => {
	return Course.find({}).then(result =>{
		return result
	})
}


//Get all courses Sir Earl Answer
/*
module.exports.getAll = () => {
	return Course.find({}).then((course, error) => {
		if (error){
			return error
		}
		return courses
	})
}
*/

/*
//Archive course
module.exports.archiveCourse = (course_id, new_status) => {

	let archive_course = {
		isActive: new_status.isActive
	}

	return Course.findByIdAndUpdate(course_id, archive_course).then((modified_courseStatus, error) => {
		
		if(error){
			return error
		}

		return {
			message: "Course archived successfully",
			data: modified_courseStatus
		}
	})
}
*/



module.exports.archiveCourse = (course_id, new_status) => {

	let archive_course = {
		isActive: false
	}

	return Course.findByIdAndUpdate(course_id, archive_course).then((modified_courseStatus, error) => {
		
		if(error){
			return error
		}

		return {
			message: "Course archived successfully",
			data: modified_courseStatus
		}
	})
}


//sir earl answer
/*
module.exports.archiveCourse = (course_id) => {
	if(error){
			return error
		}

	course.isActive = false

	return course.save().then((archived_courses, error) => {
		if(error){
			return error
		}

		return archived_courses
	})
}


*/